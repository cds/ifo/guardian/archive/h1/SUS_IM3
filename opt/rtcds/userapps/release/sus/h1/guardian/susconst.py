#+# ramp time for output gains
default_ramp_time = 2.0
align_ramp_time = 5.0
coildriver_ramp_time = 8.0

use_olDamp = False
ol_off_threshold = 500
ol_on_threshold = 500

alignment_dofs = ['P', 'Y']
misalign_values = {'BS': {'P': 12.5523, 'Y': -19.1541},
                   'ETMX': {'P': 10.15, 'Y': -8.45},
                   'ETMY': {'P': 10.15, 'Y': 8.45},
                   'IM1': {'P': 333.0, 'Y': 333.0},
                   'IM2': {'P': -700.0, 'Y': -520.0},
                   'IM3': {'P': 555.0, 'Y': 555.0},
                   'IM4': {'P': 555.0, 'Y': 555.0},
                   'ITMX': {'P': -300.0, 'Y': -16.9},
                   'ITMY': {'P': 400.0, 'Y': 100.0},
                   'MC1': {'P': 0.0, 'Y': 0.0},
                   'MC2': {'P': 0.0, 'Y': -1000.0},
                   'MC3': {'P': 2000.0, 'Y': 2000.0},
                   'OFI': {'Y': 0.0},
                   'OM1': {'P': 0.0, 'Y': 0.0},
                   'OM2': {'P': 0.0, 'Y': 0.0},
                   'OM3': {'P': 800.0, 'Y': 1000.0},
                   'OMC': {'P': 100.0, 'Y': 20.0},
                   'OPO': {'P': 0.0, 'Y': 0.0},
                   'PR2': {'P': 500.0, 'Y': 500.0},
                   'PR3': {'P': -705.7, 'Y': -3338.0},
                   'PRM': {'P': 681.0, 'Y': -5914.0},
                   'RM1': {'P': 0.0, 'Y': 0.0},
                   'RM2': {'P': 0.0, 'Y': 100.0},
                   'SR2': {'P': 200.0, 'Y': 300.0},
                   'SR3': {'P': -555.0, 'Y': -152.3},
                   'SRM': {'P': -1400.0, 'Y': 1290.0},
                   'TMSX': {'P': -8.6, 'Y': 31.6},
                   'TMSY': {'P': 40.1, 'Y': 34.6},
                   'ZM1': {'P': 100.0, 'Y': 200.0},
                   'ZM2': {'P': 100.0, 'Y': 200.0},
                   'ZM3': {'P': 100.0, 'Y': 200.0},
                   'ZM4': {'P': 100.0, 'Y': 200.0},
                   'ZM5': {'P': 100.0, 'Y': 200.0},
                   'ZM6': {'P': 100.0, 'Y': 200.0},
                   'FC1': {'P': 100.0, 'Y': 200.0},
                   'FC2': {'P': 100.0, 'Y': 200.0},
                   
}

wd_threshold_high = 80000
wd_threshold_nominal = 25000

wd_thresholds = {"BS": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ETMX": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ETMY": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "FC1": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "IM1": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "IM2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "IM3": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "IM4": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ITMX": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ITMY": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "MC1": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "MC2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "MC3": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OFI": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OM1": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OM2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OM3": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OMC": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "OPO": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "PR2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "PR3": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "PRM": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "RM1": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "RM2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "SR2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "SR3": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "SRM": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "TMSX": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "TMSY": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ZM1": {'high': 250000, 'nominal': 100000},
                 "ZM2": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ZM3": {'high': 500000, 'nominal': 150000},
                 "ZM4": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ZM5": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
                 "ZM6": {'high': wd_threshold_high, 'nominal': wd_threshold_nominal},
}


# From LLO
#Coil drive state definitions
quad_matrix = {'L1':{'l':0.25,'p':3.8462,'y':3.8462},'L2':{'l':0.25,'p':3.5355,'y':3.5355}}
bs_matrix = {'M2':{'l':0.25,'p':3.5361,'y':3.5361}}
hsts_matrix = {'M3':{'l':0.25,'p':5.2382,'y':5.2382}}

#Unlisted suspension don't have ramping matrices, so can't rotate off a given quadrant, so need for them here.
matrix_values = {'ETMX': quad_matrix,
                 'ETMY': quad_matrix,
                 'ITMX': quad_matrix,
                 'ITMY': quad_matrix,
                 'BS': bs_matrix,
                 'MC1': hsts_matrix,
                 'MC2': hsts_matrix,
                 'MC3': hsts_matrix,
                 'PRM': hsts_matrix,
                 'PR2': hsts_matrix,
                 'SRM': hsts_matrix,
                 'SR2': hsts_matrix,
}


# Updated so it should be consistent with what we actually do here at LHO
#Initial state at DOWN we want suspension in
coilstate_lock_acq = {'BS': {'M2':2},
                  'ETMX': {'L1':2, 'L2':2, 'L3':1}, #From ISC_LOCK DOWN
                  'ETMY': {'L1':2, 'L2':2, 'L3':2}, #From ISC_LOCK DOWN
                  'ITMX': {'L1':3, 'L2':2,},
                  'ITMY': {'L1':3, 'L2':2,},
                  'IM1': {},
                  'IM2': {},
                  'IM3': {},
                  'IM4': {},
                  'MC1': {},
                  'MC2': {},
                  'MC3': {},
                  'OFI': {},
                  'OM1': {},
                  'OM2': {},
                  'OM3': {},
                  'OMC': {},
                  'OPO': {},
                  'PR2': {'M3':2},
                  'PR3': {},
                  'PRM': {'M2':3, 'M3':2},
                  'RM1': {},
                  'RM2': {},
                  'SR2': {'M3':2},
                  'SR3': {},
                  'SRM': {'M2':3, 'M3':2},
                  'TMSX': {},
                  'TMSY': {},
                  'ZM1': {},
                  'ZM2': {}
}

#FIXME: After we lock DRMI we move the PRM and SRM M3 stages to a bio state of 1, this isnt captured in the below table

# Also updated, mainly from SLOWNOISE_COILDRIVERS ISC_LOCK state
#First optional transition state
coilstate_first_low_noise = {'BS': {'M2':3},
                  'ETMX': {'L2':1,},
                  'ETMY': {'L2':1,},
                  'ITMX': {'L2':1,},
                  'ITMY': {'L2':1,},
                  'MC1': {},
                  'MC2': {},
                  'MC3': {},
                  'PR2': {'M3':3},
                  'PR3': {},
                  'PRM': {'M3':3},
                  'RM1': {},
                  'RM2': {},
                  'SR2': {'M3':3},
                  'SR3': {},
                  'SRM': {'M3':3},
                  'TMSX': {},
                  'TMSY': {},
                  'ZM1': {},
                  'ZM2': {}
}

#Second optional transition state #### NOT USED, its a copy from LLO
coilstate_second_low_noise = {
                  'ETMX': {'L1':3,},
                  'ETMY': {'L1':3,},
}
